class Employee  (var employeeID: String){

    fun display(){
        println("Inside Employee : Employee Id : ${this.employeeID}")
    }

    fun displayEmployeeIDWithLamda(func: (s: String) -> Unit){
        func(employeeID)
    }
}