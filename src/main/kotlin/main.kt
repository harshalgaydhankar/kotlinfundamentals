fun main(args:Array<String>){
    println("Hello, world")

    var harshal = Employee("001")
    println("Inside main : Employee ID ${harshal.employeeID}")//string interpolation
    harshal.employeeID= "007"
    println("Inside main : Employee ID ${harshal.employeeID}")

    harshal.display();

    harshal.displayEmployeeIDWithLamda(::printEmployeeID)

}

fun printEmployeeID(id: String){
    println("Inside Lamda Function : Employee ID $id")
}